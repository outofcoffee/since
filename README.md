# Since: Generate incremental release notes from  your Git commit history

[![Build Status](https://drone.io/bitbucket.org/outofcoffee/since/status.png)](https://drone.io/bitbucket.org/outofcoffee/since/latest)

## Why would I want this?
Do you use Git and have to generate release notes for your project every time you release a new version?

Wouldn't it be better if you could just generate the release notes automatically based on your commit history? Better yet, shouldn't the next release *remember* what you released last time so it only gives you what's changed? 

## An example
It's release time. Let's generate release notes from the current working directory:

    $ cd /path/to/my/git/repo
    $ since -l -m
    
    Added an awesome new feature.
    Added the ability to rotate images by irrational numbers.
    Fixed a bug that could cause spontaneous combustion.

OK - release notes done. Time to update the last release marker for next time:

    $ since -u
    
    Updated last release hash to 'dd638b7e67da82905aa7eb93ef03e2cd6b1faf86'

That was easy!

### Some time later...
After doing more work, it's time for a new release. Let's generate some release notes:

    $ since -l -m
    
    Fixed an issue that could result in pixel inversion.
    Enabled awesome mode.

Oh look - it only showed the changes since the last release :-) This is obviously a very simple workflow, but when you consider how this might work with multiple branches it can be a very powerful tool.

### Workflow

Here's an example workflow we use (all automated after step 1, of course):

1. Work on our project, using feature branches (see git-flow), then merging them into the ``develop`` branch.
2. Generate release notes as above, storing them in an environment variable
3. Run our project's automatic build and upload script, passing the environment variable for the 'change list'
4. Our testers receive a new build notifying them of the changes made in the new version
5. Update the marker as above
6. Repeat

## Getting started
### Prerequisites
- Make sure you have **JDK6+** installed. Prior versions may work but are not tested.
- The tool works with Git repositories.

First, clone this repository:

    git clone https://bitbucket.org/outofcoffee/since.git
    cd ./since

You now have two options to start using the tool:

### Option A: One liner
If you don't want to go through the step by step instructions, you can use this one liner:

On *NIX and OS X:

     sh ./bin/setup-since.sh

On Windows:

    bin\setup-since.bat

### Option B: Step by step
Build the JAR:

    ./mvnw clean package
    
    ...
    [INFO] ------------------------------------------------------------------------
    [INFO] BUILD SUCCESS
    [INFO] ------------------------------------------------------------------------
    [INFO] Total time: 3.722s

The JAR is built to the ``target`` subdirectory. To run it, we can use the convenience script in the ``bin`` directory.

On *NIX and OS X only: first, let's make it executable:

    cd bin
    chmod +x ./since

Now you can do this:

    ./since [options]
    
If you want to make the tool accessible everywhere, you can add the path to the ``bin`` folder to your ``PATH`` in your ``~/.bash_profile`` file. The 'one liner' option above will do this for you.

You're good to go. Check out the *Usage* and *More examples* sections.

## How does it work?
Since stores your release metadata, like the last release marker, in your Git repository. This information lives in a folder named ``.since`` in the root of the repository. When the metadata changes, Since automatically updates the information in the repository. Whilst Since will commit changes to its own metadata, it won't push any changes to your remotes. You're still in charge.

## Usage
     --afterCommit (-c) VAL      : List changes since after this commit
     --afterMarker (-m)          : List changes since the last release marker
     --debug                     : Log at DEBUG level
     --help (-h)                 : Display usage only
     --includeAuthor (-a)        : Whether to include author
     --keepTrailingNewLines (-n) : Whether to keep trailing new lines (stripped by default)
     --listChanges (-l)          : List changes
     --repoDir (-r) FILE         : The path to the repository
     --updateReleaseMarker (-u)  : Update the last release marker to HEAD
     
Protip: you can generate this output by specifying the ``-h`` option.

## More examples
The examples are split into different sections for generating notes and updating the marker.

### Generating release notes
    
#### List the changes for a Git repo in the current directory since the last release marker
This is the most common usage of the tool.

    $ since -l -m
    
    Excluded generated file from VCS.
    Inverted option to keep trailing new lines.
    Now packaging as uberjar for simple usage.
        
#### List the changes showing the author
Show the author name next to each change.

    $ since -l -m -a
    
    [outofcoffee] Excluded generated file from VCS
    [outofcoffee] Inverted option to keep trailing new lines.
    [outofcoffee] Now packaging as uberjar for simple usage.
    
#### List the changes for a Git repo in another directory 
Specify the path to the Git repo if it's not the working directory.

    $ since -l -m -r /path/to/your/git/repo
    
    Excluded generated file from VCS.
    Inverted option to keep trailing new lines.
    Now packaging as uberjar for simple usage.
    
#### List the changes since a commit
Lists all changes since after the given commit hash.

    $ since -l -c 1d5dee15f518403237a222432ec31b25b94c0c76
    
    Excluded generated file from VCS.
    
#### List all the changes for a Git repo in the current working directory
It's not very likely you're going to use this as it lists all changes on the current branch.

    $ since -l
    
    Excluded generated file from VCS.
    Inverted option to keep trailing new lines.
    Now packaging as uberjar for simple usage.
    ...
    Initial commit.

### Updating the release marker
Don't forget to update the marker when you're happy to consider the release complete, so that next time you generate notes, you'll only get the difference from this point onwards.

#### Update the release marker for a Git repo in the current working directory
    $ since -u

    Updated last release hash to 'dd638b7e67da82905aa7eb93ef03e2cd6b1faf86'

#### Update the release marker for a Git repo in another directory
    $ since -u -r /path/to/your/git/repo

    Updated last release hash to 'dd638b7e67da82905aa7eb93ef03e2cd6b1faf86'

## Contact / Author
- Pete Cornish (outofcoffee at gmail dot com)

## Continuous Integration
We use [drone.io](https://drone.io/bitbucket.org/outofcoffee/since)
