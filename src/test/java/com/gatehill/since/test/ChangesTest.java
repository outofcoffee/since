package com.gatehill.since.test;

import com.gatehill.since.Changes;
import com.gatehill.since.RepoOptions;
import com.gatehill.since.exception.SinceException;
import org.apache.commons.compress.archivers.ArchiveInputStream;
import org.apache.commons.compress.archivers.ArchiveStreamFactory;
import org.apache.commons.compress.archivers.zip.ZipArchiveEntry;
import org.apache.commons.compress.utils.IOUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.output.TeeOutputStream;
import org.apache.commons.lang3.StringUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.*;

import static org.junit.Assert.*;

/**
 * Tests for {@link Changes}.
 *
 * @author pete
 */
public class ChangesTest {
    /**
     * The hash of the first commit from the repository at {@link #testRepo}.
     */
    private static final String HASH_TO_START_FROM = "1d5dee15f518403237a222432ec31b25b94c0c76";

    /**
     * Name of the repository subdirectory within the unzipped directory.
     */
    private static final String REPO_DIR = "repo";

    /**
     * Invalid path.
     */
    private static final String INVALID_PATH = "/invalid/path/to/repo";

    /**
     * Path to a Git repository.
     */
    private File testRepo;

    /**
     * Unit under test.
     */
    private Changes changes;

    /**
     * Obtain a path to a Git repository.
     *
     * @return path to the root of the repository
     * @throws Exception
     */
    private File getTestRepo() throws Exception {
        // create output dir
        final File outputDir = File.createTempFile("since-repo", null);
        if (outputDir.exists()) {
            if (!outputDir.delete()) {
                throw new IOException("Unable to delete directory: " + outputDir);
            }
        }
        if (!outputDir.mkdir()) {
            throw new IOException("Unable to make directory: " + outputDir);
        }

        final InputStream is = ChangesTest.class.getResourceAsStream("/repo.zip");
        final ArchiveInputStream in = new ArchiveStreamFactory().createArchiveInputStream("zip", is);
        try {
            // create files and folders from archive
            ZipArchiveEntry entry = (ZipArchiveEntry) in.getNextEntry();
            while (null != entry) {
                final File entryName = new File(outputDir, entry.getName());
                if (entry.isDirectory()) {
                    if (!entryName.mkdirs()) {
                        throw new IOException("Unable to make directory: " + outputDir);
                    }

                } else {
                    final OutputStream out = new FileOutputStream(entryName);
                    try {
                        IOUtils.copy(in, out);
                    } finally {
                        IOUtils.closeQuietly(out);
                    }
                }

                entry = (ZipArchiveEntry) in.getNextEntry();
            }

        } finally{
            IOUtils.closeQuietly(is);
            IOUtils.closeQuietly(in);
        }

        // clean up after test
        outputDir.deleteOnExit();

        // subdirectory name
        return new File(outputDir, REPO_DIR);
    }

    /**
     * Some of the tests change state so we need to get a new repo every time.
     *
     * @throws Exception
     */
    @Before
    public void before() throws Exception {
        changes = new Changes();
        testRepo = getTestRepo();
    }

    /**
     * Attempt to clean up after test.
     *
     * @throws IOException
     */
    @After
    public void after() throws IOException {
        FileUtils.deleteDirectory(testRepo);
    }

    /**
     * Get all changes.
     * <p/>
     * By default, removal of trailing new lines is enabled.
     *
     * @throws Exception
     */
    @Test
    public void testGetChanges_Success_All() throws Exception {
        // test data
        // none

        // call
        final String[] actual = changes.getChanges(testRepo);

        // assert output
        assertNotNull("Changes should be returned", actual);
        assertEquals("Should be two changes", 3, actual.length);
        assertEquals("Verify first change message", "Third commit", actual[0]);
        assertEquals("Verify first change message", "Second commit", actual[1]);
        assertEquals("Verify second change message", "Initial commit", actual[2]);
    }

    /**
     * Get changes starting from after a particular commit, without including author and without stripping
     * trailing new lines.
     *
     * @throws Exception
     */
    @Test
    public void testGetChanges_Success_AfterCommit_NoAuthor() throws Exception {
        // test data
        // none

        // call
        final String[] actual = changes.getChanges(new RepoOptions(testRepo, HASH_TO_START_FROM), false, true);

        // assert output
        assertNotNull("Changes should be returned", actual);
        assertEquals("Should be two changes", 2, actual.length);
        assertEquals("Verify first change message", "Third commit\n", actual[0]);
        assertEquals("Verify second change message", "Second commit\n", actual[1]);
    }

    /**
     * Get changes starting from after a particular commit, including author and without stripping
     * trailing new lines.
     *
     * @throws Exception
     */
    @Test
    public void testGetChanges_Success_AfterCommit_WithAuthor() throws Exception {
        // test data
        // none

        // call
        final String[] actual = changes.getChanges(new RepoOptions(testRepo, HASH_TO_START_FROM), true, true);

        // assert output
        assertNotNull("Changes should be returned", actual);
        assertEquals("Should be one change", 2, actual.length);
        assertEquals("Verify first change message", "[outofcoffee] Third commit\n", actual[0]);
        assertEquals("Verify second change message", "[outofcoffee] Second commit\n", actual[1]);
    }

    /**
     * Verify that a {@link com.gatehill.since.exception.SinceException} is thrown, wrapping an internal exception.
     *
     * @throws Exception
     */
    @Test
    public void testGetChanges_Error_InvalidRepoPath() throws Exception {
        // test data
        // none

        // call
        boolean exThrown = false;
        try {
            changes.getChanges(new File(INVALID_PATH));
            fail(SinceException.class + " should be thrown");

        } catch (SinceException e) {
            assertNotNull("Ensure the exception cause is not swallowed", e.getCause());
            exThrown = true;
        }

        // assert output
        assertTrue(SinceException.class + " should be thrown", exThrown);
    }

    /**
     * Update the release marker to HEAD.
     *
     * @throws Exception
     */
    @Test
    public void testUpdateReleaseMarker_StoreChanges_Success() throws Exception {
        // test data
        final RepoOptions repoOptions = new RepoOptions(testRepo);

        // sanity check
        final String lastReleaseHash = changes.getLastReleaseHash(repoOptions);
        assertTrue("Last release hash should not be set", StringUtils.isBlank(lastReleaseHash));

        // call
        changes.updateReleaseMarker(testRepo);

        // assert output
        final String newReleaseHash = changes.getLastReleaseHash(repoOptions);
        assertTrue("Last release hash should be set", StringUtils.isNotBlank(newReleaseHash));
    }

    /**
     * Invoke command line main method with a path to the repository. All changes should be printed to stdout.
     *
     * @throws Exception
     */
    @Test
    public void testMain_Help_Success() throws Exception {
        // test data
        final ByteArrayOutputStream out = new ByteArrayOutputStream();
        System.setOut(new PrintStream(new TeeOutputStream(System.out, out)));

        final ByteArrayOutputStream err = new ByteArrayOutputStream();
        System.setErr(new PrintStream(new TeeOutputStream(System.err, err)));

        final String[] args = {
            "-h"
        };

        // call
        Changes.main(args);

        // assert output
        assertEquals("No errors should have been emitted", 0, err.size());
        assertNotSame("Output should be emitted", 0, out.size());

        final String output = new String(out.toByteArray());
        assertNotNull("Output should not be empty", output);
        assertTrue("Error should contain explanation", output.contains("--help"));
    }

    /**
     * Invoke command line main method with a path to the repository. All changes should be printed to stdout.
     *
     * @throws Exception
     */
    @Test
    public void testMain_ListChanges_Success() throws Exception {
        // test data
        final ByteArrayOutputStream out = new ByteArrayOutputStream();
        System.setOut(new PrintStream(new TeeOutputStream(System.out, out)));

        final ByteArrayOutputStream err = new ByteArrayOutputStream();
        System.setErr(new PrintStream(new TeeOutputStream(System.err, err)));

        final String[] args = {
            "-l",
            "-r", testRepo.getAbsolutePath()
        };

        // call
        Changes.main(args);

        // assert output
        assertEquals("No errors should have been emitted", 0, err.size());
        assertNotSame("Output should be emitted", 0, out.size());

        String output = new String(out.toByteArray());
        assertNotNull("Output should not be empty", output);

        // remove carriage return on windows platforms
        output = output.replaceAll("\r", "");

        final String[] lines = output.split("\n");
        assertEquals("Should be two changes", 3, lines.length);
        assertEquals("Verify first change message", "Third commit", lines[0]);
        assertEquals("Verify first change message", "Second commit", lines[1]);
        assertEquals("Verify second change message", "Initial commit", lines[2]);
    }

    /**
     * Invoke command line main method with an invalid repository path. Error message should be printed to stderr.
     *
     * @throws Exception
     */
    @Test
    public void testMain_Error() throws Exception {
        // test data
        final ByteArrayOutputStream out = new ByteArrayOutputStream();
        System.setOut(new PrintStream(new TeeOutputStream(System.out, out)));

        final ByteArrayOutputStream err = new ByteArrayOutputStream();
        System.setErr(new PrintStream(new TeeOutputStream(System.err, err)));

        // s is not a valid option
        final String[] args = {
            "--invalidOption", "EXAMPLE"
        };

        // call
        Changes.main(args);

        // assert output
        assertEquals("No output should have be emitted", 0, out.size());
        assertNotSame("Errors should have been emitted", 0, err.size());

        final String errors = new String(err.toByteArray());
        assertNotNull("Errors should not be empty", errors);
        assertTrue("Error should contain explanation", errors.contains("\"--invalidOption\" is not a valid option"));
    }
}
