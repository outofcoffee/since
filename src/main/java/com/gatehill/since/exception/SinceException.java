package com.gatehill.since.exception;

/**
 * @author pete
 */
public class SinceException extends Exception {
    public SinceException(String message, Exception e) {
        super(message, e);
    }

    public SinceException(String message) {
        super(message);
    }
}
