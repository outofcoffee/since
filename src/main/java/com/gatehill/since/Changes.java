package com.gatehill.since;

import ch.qos.logback.classic.Level;
import com.gatehill.since.exception.SinceException;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.LogCommand;
import org.eclipse.jgit.api.StatusCommand;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.lib.Constants;
import org.eclipse.jgit.lib.ObjectId;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.storage.file.FileRepositoryBuilder;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.Option;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Gets the list of changes from a Git repository, optionally starting from after a specified commit.
 * <p/>
 * By 'changes' we mean a list of the commit messages, authors, etc.
 *
 * @author pete
 */
public class Changes {
    private static final Logger LOGGER = LoggerFactory.getLogger(Changes.class);
    private static final String METADATA_DIR = ".since";
    private static final String MARKER_FILE = "last-release";
    private static final String MESSAGE_NEW_RELEASE = "New release";

    @Option(name = "--debug", usage = "Log at DEBUG level")
    private boolean logDebug;

    @Option(name = "--help", aliases = { "-h" }, usage = "Display usage only", forbids = { "-l", "-u", "-r", "-c", "-a", "-n" })
    private boolean displayHelp;

    @Option(name = "--listChanges", aliases = { "-l" }, usage = "List changes", forbids = { "-h", "-u" })
    private boolean listChanges;

    @Option(name = "--updateReleaseMarker", aliases = { "-u" }, usage = "Update the last release marker to HEAD", forbids = { "-h", "-l", "-c", "-a", "-n" })
    private boolean updateReleaseMarker;

    @Option(name = "--repoDir", aliases = { "-r" }, usage = "The path to the repository", forbids = { "-h" })
    private File repo;

    @Option(name = "--afterCommit", aliases = { "-c" }, usage = "List changes since after this commit", forbids = { "-h", "-u" })
    private String afterCommitHash;

    @Option(name = "--afterMarker", aliases = { "-m" }, usage = "List changes since the last release marker", forbids = { "-h", "-u" })
    private boolean afterMarker;

    @Option(name = "--includeAuthor", aliases = { "-a" }, usage = "Whether to include author", forbids = { "-h", "-u" })
    private boolean includeAuthors;

    @Option(name = "--keepTrailingNewLines", aliases = { "-n" }, usage = "Whether to keep trailing new lines (stripped by default)", forbids = { "-h", "-u" })
    private boolean keepTrailingNewLines;

    /**
     * Parse arguments and get changes.
     *
     * @param args command line arguments
     * @throws SinceException
     */
    public static void main(String[] args) throws SinceException {
        final Changes changes = new Changes();
        final CmdLineParser parser = new CmdLineParser(changes);
        try {
            parser.parseArgument(args);

            // override log level
            if (changes.logDebug) {
                final Logger rootLogger = LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME);
                if (rootLogger instanceof ch.qos.logback.classic.Logger) {
                    ((ch.qos.logback.classic.Logger) rootLogger).setLevel(Level.DEBUG);
                }
            }

            // begin execution
            if (changes.displayHelp || 0 == args.length) {
                parser.printUsage(System.out);
            } else if (changes.listChanges || changes.updateReleaseMarker) {
                changes.run();
            }

        } catch (CmdLineException e) {
            // handling of wrong arguments
            System.err.println(e.getMessage());
            parser.printUsage(System.err);
        }
    }

    /**
     * Print the changes to stdout.
     *
     * @throws SinceException
     */
    private void run() throws SinceException {
        // use working dir if not specified
        if (null == repo) {
            repo = new File(System.getProperty("user.dir"));
        }

        final RepoOptions repoOptions = new RepoOptions(repo, afterCommitHash);
        if (updateReleaseMarker) {
            // update last release marker
            updateReleaseMarker(repoOptions);

        } else if (listChanges) {
            if (afterMarker) {
                LOGGER.debug("Attempting to use commit hash after last release");
                final String lastReleaseHash = getLastReleaseHash(repoOptions);
                repoOptions.setSinceCommitHash(lastReleaseHash);
            }

            // print changes
            for (String change : getChanges(repoOptions, includeAuthors, keepTrailingNewLines)) {
                System.out.println(change);
            }

        } else {
            throw new SinceException("No operation specified. Please examine the usage instructions.");
        }
    }

    /**
     * Convenience method for {@link #getChanges(RepoOptions, boolean, boolean)}, passing <code>null</code>
     * for the afterCommitHash, and <code>false</code> for includeAuthors and
     * <code>false</code> for keepTrailingNewLines.
     *
     * @param repo path to the root of a Git repository
     * @return the list of changes
     * @throws SinceException
     */
    public String[] getChanges(File repo) throws SinceException {
        return getChanges(new RepoOptions(repo), false, false);
    }

    /**
     * Get the list of changes from the given repository, starting from after the specified commit,
     * optionally with authors.
     *
     * @param repoOptions          repository options
     * @param includeAuthors       whether to include author in change text
     * @param keepTrailingNewLines whether to retain trailing new lines
     * @return the list of changes
     * @throws SinceException
     */
    public String[] getChanges(RepoOptions repoOptions, boolean includeAuthors, boolean keepTrailingNewLines) throws SinceException {
        try {
            LOGGER.debug("Listing changes for repository: {}", repoOptions);

            final FileRepositoryBuilder builder = new FileRepositoryBuilder();
            final Repository repository = builder.setGitDir(new File(repoOptions.getRepo(), Constants.DOT_GIT))
                .readEnvironment() // scan environment GIT_* variables
                .findGitDir() // scan up the file system tree
                .build();

            final Git git = Git.wrap(repository);
            final LogCommand logCommand = git.log();

            // start from after the given commit
            if (StringUtils.isNotBlank(repoOptions.getSinceCommitHash())) {
                logCommand.addRange(repository.resolve(repoOptions.getSinceCommitHash()), repository.resolve(Constants.HEAD));
            }

            // build change strings from each commit
            final List<String> changes = new ArrayList<String>();
            for (RevCommit commit : logCommand.call()) {
                final StringBuilder change = new StringBuilder();

                if (includeAuthors) {
                    change.append("[");
                    change.append(commit.getAuthorIdent().getName());
                    change.append("] ");
                }

                // commit message, optionally stripping trailing new lines
                String fullMessage = commit.getFullMessage();
                if (!keepTrailingNewLines && fullMessage.endsWith("\n")) {
                    fullMessage = fullMessage.substring(0, fullMessage.length() - 1);
                }

                change.append(fullMessage);
                changes.add(change.toString());
            }

            return changes.toArray(new String[changes.size()]);

        } catch (Exception e) {
            throw new SinceException("Error getting changes for repository: " + repoOptions, e);
        }
    }

    /**
     * @param repoOptions repository options
     * @return the last release hash for the repository
     * @throws SinceException
     */
    public String getLastReleaseHash(RepoOptions repoOptions) throws SinceException {
        try {
            final File markerFile = getMarkerFile(repoOptions, false);
            if (markerFile.exists()) {
                return FileUtils.readFileToString(markerFile);
            }
            return null;

        } catch (Exception e) {
            throw new SinceException("Error getting last release hash from repository: " + repoOptions);
        }
    }

    /**
     * Convenience method for {@link #updateReleaseMarker(RepoOptions)}, passing <code>null</code>
     * for the afterCommitHash.
     *
     * @param repo path to the root of a Git repository
     * @throws SinceException
     */
    public void updateReleaseMarker(File repo) throws SinceException {
        updateReleaseMarker(new RepoOptions(repo));
    }

    /**
     * Update the release marker to HEAD. This should only be attempted if the working copy is clean, or else a
     * {@link com.gatehill.since.exception.SinceException} will be thrown.
     *
     * @param repoOptions repository options
     * @throws SinceException
     */
    public void updateReleaseMarker(RepoOptions repoOptions) throws SinceException {
        try {
            final FileRepositoryBuilder builder = new FileRepositoryBuilder();
            final Repository repository = builder.setGitDir(new File(repoOptions.getRepo(), Constants.DOT_GIT))
                .readEnvironment() // scan environment GIT_* variables
                .findGitDir() // scan up the file system tree
                .build();

            final Git git = Git.wrap(repository);

            // don't continue if working copy is not clean
            assertNoPendingChanges(git, repoOptions);

            // update the marker
            updateMarker(git, repository.resolve(Constants.HEAD), repoOptions);

        } catch (Exception e) {
            throw new SinceException("Error updating release marker for repository: " + repoOptions, e);
        }
    }

    /**
     * Update the marker file to the given marker.
     *
     * @param git         repo instance
     * @param newMarker   object ID for the new release marker
     * @param repoOptions repository options
     * @throws IOException
     * @throws SinceException
     */
    private void updateMarker(Git git, ObjectId newMarker, RepoOptions repoOptions) throws IOException, SinceException, GitAPIException {
        final String newReleaseHash = ObjectId.toString(newMarker);

        final File markerFile = getMarkerFile(repoOptions, true);
        final String lastReleaseHash = FileUtils.readFileToString(markerFile);

        LOGGER.debug("Updating last release hash from '{}' to '{}'", lastReleaseHash, newReleaseHash);
        FileUtils.writeStringToFile(markerFile, newReleaseHash);

        // add and commit marker
        addAndCommit(git, METADATA_DIR + File.separatorChar + MARKER_FILE, MESSAGE_NEW_RELEASE);
        LOGGER.info("Updated last release hash to '{}'", newReleaseHash);
    }

    /**
     * Add and commit the given file to the repository, with the specified message.
     *
     * @param git         repo instance
     * @param filePattern files to add
     * @param message     the commit message
     * @throws GitAPIException
     */
    private void addAndCommit(Git git, String filePattern, String message) throws GitAPIException {
        git.add()
            .addFilepattern(filePattern)
            .call();

        git.commit()
            .setMessage(message)
            .call();
    }

    /**
     * Get the last release marker file, optionally creating an empty file if it doesn't exist.
     *
     * @param repoOptions      repository options
     * @param createIfNotExist create the last release marker file if it doesn't exist
     * @return the last release marker file
     * @throws SinceException
     * @throws IOException
     */
    private File getMarkerFile(RepoOptions repoOptions, boolean createIfNotExist) throws SinceException, IOException {
        final File metaDir = new File(repoOptions.getRepo(), METADATA_DIR);
        if (!metaDir.exists() && createIfNotExist) {
            LOGGER.debug("Creating new metadata directory: {}", metaDir);

            if (!metaDir.mkdir()) {
                throw new SinceException("Unable to create metadata directory: " + metaDir);
            }
        }

        final File markerFile = new File(metaDir, MARKER_FILE);
        if (!markerFile.exists() && createIfNotExist) {
            LOGGER.debug("Release marker not found - creating");

            if (!markerFile.createNewFile()) {
                throw new SinceException("Unable to create release marker: " + markerFile);
            }
        }

        return markerFile;
    }

    /**
     * Assert that the working copy is clean. If not, a {@link com.gatehill.since.exception.SinceException} will
     * be thrown.
     *
     * @param git         repo instance
     * @param repoOptions repository options
     * @throws GitAPIException
     * @throws SinceException
     */
    private void assertNoPendingChanges(Git git, RepoOptions repoOptions) throws GitAPIException, SinceException {
        final StatusCommand statusCommand = git.status();

        if (!statusCommand.call().isClean()) {
            throw new SinceException(String.format("The repository %s has outstanding changes", repoOptions));
        }
    }
}
