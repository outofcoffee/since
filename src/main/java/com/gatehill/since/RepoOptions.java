package com.gatehill.since;

import java.io.File;

/**
 * Holds repository options.
 *
 * @author pete
 */
public class RepoOptions {
    private final File repo;
    private String sinceCommitHash;

    /**
     * @param repo            path to the root of a Git repository
     * @param sinceCommitHash start from after a particular commit - can be <code>null</code>
     */
    public RepoOptions(File repo, String sinceCommitHash) {
        this(repo);
        this.sinceCommitHash = sinceCommitHash;
    }

    public RepoOptions(File repo) {
        this.repo = repo;
    }

    @Override
    public String toString() {
        return "RepoOptions{" +
            "repo=" + repo +
            ", sinceCommitHash='" + sinceCommitHash + '\'' +
            '}';
    }

    public File getRepo() {
        return repo;
    }

    public String getSinceCommitHash() {
        return sinceCommitHash;
    }

    public void setSinceCommitHash(String sinceCommitHash) {
        this.sinceCommitHash = sinceCommitHash;
    }
}
