#!/bin/sh

set -e

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
SINCE_HOME="$( cd ${SCRIPT_DIR}/.. && pwd )"

# build from source
echo ""
echo "Building from source"
cd ${SINCE_HOME}
sh ./mvnw clean package

# set up script
echo ""
echo "Setting up executable"
chmod +x ${SCRIPT_DIR}/since

echo ""
echo "Add to ~./bash_profile? [y/n]"
read ADD_TO_BASH_PROFILE

if [[ "y" == "${ADD_TO_BASH_PROFILE}" ]]; then
    echo ""
    echo "Adding to ~/.bash_profile"

    echo "" >> ~/.bash_profile
    echo "" >> ~/.bash_profile
    echo "export SINCE_HOME=${SCRIPT_DIR}" >> ~/.bash_profile
    echo "export PATH=\$PATH:\${SINCE_HOME}" >> ~/.bash_profile

    echo "Done. You should run: source ~/.bash_profile"
fi

echo ""
echo "Setup complete"
