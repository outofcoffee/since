@echo off

SET SCRIPT_DIR=%~dp0
SET SINCE_HOME=%SCRIPT_DIR%\..
SET JAVA_OPTS=%JAVA_OPTS%

:: run the tool
java -jar %SINCE_HOME%\target\since.jar %JAVA_OPTS% %*
