@echo off

SET SCRIPT_DIR=%~dp0
SET SINCE_HOME=%SCRIPT_DIR%\..

:: build from source
echo.
echo Building from source
cd %SINCE_HOME%
call mvnw.bat clean package

echo.
echo It is strongly recommended that you add the following to your 'Path' variable:
echo %SCRIPT_DIR%

echo.
echo Setup complete
